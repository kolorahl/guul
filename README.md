# Guul

Google's Zuul - A service for requesting and storing access tokens through
Google's OAuth 2.0 system.

## Why

Originally I had written a demo of Google Cloud's Pubsub service in Elixir and
hand-wrote a very specific client implementation of Google's OAuth 2.0 token
service. I then decided to look for existing projects in this space to determine
if I should create my own project for this purpose.

I found the wonderful library of [Goth](https://github.com/peburrows/goth). It
was pleasant and easy to integrate with, but upon looking at the internals I
found that I had some fundamental design differences between what I wanted and
what Goth provided.

In the end I didn't see anything quite like what I wanted to use, so I've gone
the route of creating my own project, **Guul**.

> There is no Goth, only Guul.
>
> _Ghostbusters or something_

## Configuration

The configuration uses one of two variables to receive its data. The first, and
generally simpler approach, is to use the `:file` key to supply a path to the
Google-provided JSON keyfile. The second is to supply JSON-data directly using
the `:json` key.

```elixir
config :guul, file: "relative/path/to/keyfile.json"

# OR

config :guul, json: %{"project_id" => "...", "private_key" => "...", ...}
```

Notice that the `:json` key requires a **parsed** JSON payload, not the JSON
string.

## Application vs Stand-alone

There are two ways to use Guul: application or stand-alone.

### Application Mode

Application mode is probably the easiest method for using Guul, especially if
your own application is long-running. Simply start the Guul application (either
in the `mix.exs` config or via `Guul.start_link`) and use `Guul.Service` for
requesting tokens.

The main benefit to using Guul in application mode is that the service module
will automatically set a timer to refresh the token shortly before it
expires. This means that long-running applications should experience little to
no interruption due to expired token use, since tokens should be getting
refreshed before they expire.

### Stand-alone

You may retrieve tokens directly, without starting the application, using the
`Guul.Token` module. The main two downsides are:

1. Configuration details are parsed with each call, meaning there's a
performance hit with this flow.
2. Tokens are not stored anywhere and are not refreshed automatically. It is up
to the calling application to refresh tokens manually at/before expiration.

So why use the stand-alone flow? If you have a script that runs infrequently, or
a short-lived application, it might be simpler to merely request a token, use
it, and then forget about it.

## API

### Retrieving Tokens

Application Mode:

```elixir
Guul.Service.get(scopes)
```

Stand-alone Mode:

```elixir
Guul.Token.get(config, scopes)
```

**Note**: The Guul service will store tokens in-memory and use the concatenated
scope string as a key for retrieving tokens. If the token does not already exist
in storage, a request for a new token is made. Otherwise it returns whatever is
currently in storage. The stand-alone flow doesn't perform any storage of keys
and will always result in an HTTP request.

### Refreshing Tokens

Application Mode:

Not necessary. This is handled by the Guul application service. However, if for
some reason the service does not manage to refresh a token in time, you may use
the following to force a token refresh:

```elixir
Guul.Service.refresh(token)
```

Stand-alone Mode:

```elixir
Guul.Token.refresh(config, token)
```

**Note**: There's no need to use the refresh function specifically. In fact, the
internal flow is to get the necessary configuration details and token scopes and
then use the regular "get token" flow to request a new token. There's nothing
special about the refresh function.

### Remove Tokens

Application Mode:

```elixir
# Remove a single token from storage
Guul.Service.drop(token)

# Remove all tokens from storage
Guul.service.clear_all
```

Stand-alone Mode:

Not applicable.

### Making API Calls

Once you have a token, use the `Guul.Request` module to make authenticated HTTP
requests.

```elixir
Guul.Request.call(token, method, url, body, headers, opts)
```

An `Authorization: Bearer <token>` header is merged into the supplied `headers`
parameter, then everything is passed to `HTTPoison.request`. There are also a
few convenience methods for simpler requests:

```elixir
Guul.Request.get(token, url, headers, opts)
Guul.Request.post(token, url, body, headers, opts)
Guul.Request.put(token, url, body, headers, opts)
Guul.Request.patch(token, url, body, headers, opts)
Guul.Request.delete(token, url, headers, opts)
```

The `headers` and `opts` parameters are optional for across all functions in the
`Guul.Request` module.
